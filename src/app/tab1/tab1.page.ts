import { Component } from '@angular/core';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})

/*
*	CONVERSIONES
*	r = √ (x2 + y2)
* 	θ = atan( y / x )
*	x = r × cos( θ )
*	y = r × sin( θ )
*/


export class Tab1Page {
	
	resultado: string = "";
 	tipo: number = 0;
 	valor1: number = 0;
 	valor2: number = 0;


	convertir(){

		if(this.tipo == 0){
			this.resultado = "Ingrese un tipo.";
		}
		//Cartesianas
		if(this.tipo == 1){
			//alert(Math.cos(this.valor2));

			let valor2: number;

			valor2 = this.valor2 * Math.PI / 180;
			valor2 = Math.cos(valor2);

			let convX: number = this.valor1 * valor2;

			valor2 = this.valor2 * Math.PI / 180;
			valor2 = Math.sin(valor2);

			let convY: number = this.valor1 * valor2;
			this.resultado = "r: " + this.valor1 + " Θ:" + this.valor2 + " \n X: " + 
							convX + " Y: " + convY;
		}

		//Polares
		if(this.tipo == 2){
			let convr: number = Math.sqrt( Math.pow(this.valor1, 2) + Math.pow(this.valor2, 2));

			let div: number = this.valor2 / this.valor1;
			//div = div * Math.PI / 180;

			let conva: number = Math.atan(div);
			this.resultado = "X: " + this.valor1 + " Y:" + this.valor2 + " \n r: " + 
							convr + " Θ: " + conva;
		}

	}

	cambiarTipo(valor){
		this.tipo = valor;
	}
}
